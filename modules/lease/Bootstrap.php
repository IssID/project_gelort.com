<?php
/**
 * Created by PhpStorm.
 * User: IssID
 * Date: 22.06.2018
 * Time: 9:26
 */
$app->getUrlManager()->addRules(
    [
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => ['leases' => 'lease/api'],
            'prefix' => 'api'
        ]
    ]
);