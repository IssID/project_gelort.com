<?php
/**
 * Created by PhpStorm.
 * User: IssID
 * Date: 22.06.2018
 * Time: 9:16
 */
namespace modules\lease\controllers\frontend;

use yii\filters\Cors;
use yii\rest\ActiveController;

class ApiController extends ActiveController
{
    public $modelClass = 'modules\lease\models\frontend\Lease';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter' ] = [
            'class' => \yii\filters\Cors::className(),
        ];
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => ['create', 'update', 'delete'],
            'rules' => [
                [
                    'actions' => ['create', 'update', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // проверяем может ли пользователь редактировать или удалить запись
        // выбрасываем исключение ForbiddenHttpException если доступ запрещен
        if ($action === 'update' || $action === 'delete') {
            if ($model->user_id !== \Yii::$app->user->id)
                throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s lease that you\'ve created.', $action));
        }
    }

}