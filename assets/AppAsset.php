<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/sidebar_nav.css',
        'css/admin_menu.css',
    ];
    public $js = [
        'js/app.js', // ангуляр приложение
        'js/controllers.js', //  для ангуляра
        'js/directives.js', // для ангуляра
        'js/services.js', // для ангуляра
        'js/sidebar_nav.js', // сайдбар в админке
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset', // bootstrap css 
        'yii\bootstrap\BootstrapPluginAsset', // bootstrap css + js
        'rmrevin\yii\fontawesome\AssetBundle', // подключаем иконки fontawesome
        'app\assets\AngularAsset', // подключаем ангуляр
    ];
}
