<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class GelortAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'gelortAssets/css/bootstrap.min.css',
        'gelortAssets/css/font-awesome.min.css',
        'gelortAssets/css/animate.min.css',
        'gelortAssets/css/owl.carousel.css',
        'gelortAssets/css/owl.theme.css',
        'gelortAssets/css/owl.transitions.css',
        'gelortAssets/css/style.css',
        'gelortAssets/css/responsive.css',
    ];
    public $js = [
//		"gelortAssets/js/jquery-1.11.3.min.js",
//        "gelortAssets/js/bootstrap.min.js",
        "gelortAssets/js/owl.carousel.min.js",
        "gelortAssets/js/jquery.stickit.min.js",
        "gelortAssets/js/menu.js",
        "gelortAssets/js/scripts.js",
    ];
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset', // bootstrap css + js
    ];
}
