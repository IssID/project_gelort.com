/*
Navicat MySQL Data Transfer

Source Server         : gelort.com
Source Server Version : 50558
Source Host           : gelort.com:3306
Source Database       : yii

Target Server Type    : MYSQL
Target Server Version : 50558
File Encoding         : 65001

Date: 2018-06-07 11:35:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `access_level` int(5) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `additional` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES ('1', '', '', '', null, '', '');
INSERT INTO `admin_menu` VALUES ('2', '', '', '', null, '', '');
INSERT INTO `admin_menu` VALUES ('3', '', '', '', null, '', '');
INSERT INTO `admin_menu` VALUES ('4', '', '', '', null, '', '');
INSERT INTO `admin_menu` VALUES ('5', '', '', '', null, '', '');
INSERT INTO `admin_menu` VALUES ('6', '', '', '', null, '', '');
INSERT INTO `admin_menu` VALUES ('7', '', '', '', null, '', '');
INSERT INTO `admin_menu` VALUES ('8', 'fa-home', 'На главную', '?', '0', 'treeview', '');
INSERT INTO `admin_menu` VALUES ('9', '', 'MAIN NAVIGATION', '', '0', 'header', '');
INSERT INTO `admin_menu` VALUES ('10', 'fa-dashboard', 'Dashboard', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('11', 'fa-circle-o', 'Dashboard v1', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('12', 'fa-circle-o', 'Dashboard v2', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('13', 'fa-files-o', 'Layout Options', '#', '0', 'treeview', 'label-primary');
INSERT INTO `admin_menu` VALUES ('14', 'fa-circle-o', 'Top Navigation', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('15', 'fa-circle-o', 'Boxed', '#', '0', 'treeview-menu', 'label-primary');
INSERT INTO `admin_menu` VALUES ('16', 'fa-circle-o', 'Fixed', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('17', 'fa-circle-o', 'Collapsed Sidebar', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('18', 'fa fa-th', 'Widgets', '#', '0', 'treeview', 'label-info');
INSERT INTO `admin_menu` VALUES ('19', 'fa-pie-chart', 'Charts', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('20', 'fa-circle-o', 'ChartJS', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('21', 'fa-circle-o', 'Morris', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('22', 'fa-circle-o', 'Flot', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('23', 'fa-circle-o', 'Inline charts', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('24', 'fa-laptop', 'UI Elements', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('25', 'fa-circle-o', 'General', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('26', 'fa-circle-o', 'Icons', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('27', 'fa-circle-o', 'Buttons', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('28', 'fa-circle-o', 'Sliders', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('29', 'fa-circle-o', 'Timeline', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('30', 'fa-circle-o', 'Modals', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('31', 'fa-edit', 'Forms', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('32', 'fa-circle-o', 'General Elements', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('33', 'fa-circle-o', 'Advanced Elements', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('34', 'fa-circle-o', 'Editors', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('35', 'fa-table', 'Tables', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('36', 'fa-circle-o', 'Simple tables', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('37', 'fa-circle-o', 'Data tables', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('38', 'fa-calendar', 'Calendar', '#', '0', 'treeview', 'label-danger');
INSERT INTO `admin_menu` VALUES ('39', 'fa-envelope', 'Mailbox', '#', '0', 'treeview', 'label-warning');
INSERT INTO `admin_menu` VALUES ('40', 'fa-folder', 'Examples', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('41', 'fa-circle-o', 'Invoice', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('42', 'fa-circle-o', 'Profile', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('43', 'fa-circle-o', 'Login', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('44', 'fa-circle-o', 'Register', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('45', 'fa-circle-o', 'Lockscreen', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('46', 'fa-circle-o', '404 Error', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('47', 'fa-circle-o', '500 Error', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('48', 'fa-circle-o', 'Blank Page', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('49', 'fa-circle-o', 'Pace Page', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('50', 'fa-share', 'Multilevel', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('51', 'fa-circle-o', 'Level One', '#', '0', 'treeview-menu', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('52', 'fa-circle-o', 'Level One', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('53', 'fa-circle-o', 'Level Two', '#', '0', 'treeview-menu', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('54', 'fa-circle-o', 'Level Two', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('55', 'fa-circle-o', 'Level Three', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('56', 'fa-circle-o', 'Level Three', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('57', 'fa-circle-o', 'Level One', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('58', 'fa-book', 'Documentation', '#', '0', 'treeview', '');
INSERT INTO `admin_menu` VALUES ('59', '', 'LABELS', '', '0', 'header', '');
INSERT INTO `admin_menu` VALUES ('60', 'fa-circle-o text-red', 'Important', '', '0', 'treeview', '');
INSERT INTO `admin_menu` VALUES ('61', 'fa-circle-o text-yellow', 'Warning', '', '0', 'treeview', '');
INSERT INTO `admin_menu` VALUES ('62', 'fa-circle-o text-aqua', 'Information', '', '0', 'treeview', '');

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `code` char(2) NOT NULL,
  `name` char(52) NOT NULL,
  `population` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('AU', 'Australia', '24016400');
INSERT INTO `country` VALUES ('BR', 'Brazil', '205722000');
INSERT INTO `country` VALUES ('CA', 'Canada', '35985751');
INSERT INTO `country` VALUES ('CN', 'China', '1375210000');
INSERT INTO `country` VALUES ('DE', 'Germany', '81459000');
INSERT INTO `country` VALUES ('FR', 'France', '64513242');
INSERT INTO `country` VALUES ('GB', 'United Kingdom', '65097000');
INSERT INTO `country` VALUES ('IN', 'India', '1285400000');
INSERT INTO `country` VALUES ('re', 'req', '32142');
INSERT INTO `country` VALUES ('RU', 'Russia', '146519759');
INSERT INTO `country` VALUES ('US', 'United States', '322976000');

-- ----------------------------
-- Table structure for discussion
-- ----------------------------
DROP TABLE IF EXISTS `discussion`;
CREATE TABLE `discussion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of discussion
-- ----------------------------
INSERT INTO `discussion` VALUES ('1', 'title test', 'text test', '2018-05-30 12:20:23', 'admin', null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `access_token` varchar(32) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'IssID', '3620eaa3c097e3a9f02d8d507854247f', '', '100', 'vlad', 'falcon', '1989-11-19', 'IccID_zxz@mail.ru');
INSERT INTO `users` VALUES ('2', 'tester', '098f6bcd4621d373cade4e832627b4f6', '', '50', null, null, null, null);
