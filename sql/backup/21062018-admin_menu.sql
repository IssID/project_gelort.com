/*
Navicat MySQL Data Transfer

Source Server         : gelort.com
Source Server Version : 50558
Source Host           : gelort.com:3306
Source Database       : yii

Target Server Type    : MYSQL
Target Server Version : 50558
File Encoding         : 65001

Date: 2018-06-21 17:41:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `access_level` int(5) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `additional` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES ('1', '', 'https://fontawesome.com/v4.7.0/icons/', 'https://fontawesome.com/v4.7.0/icons/', null, '', '');
INSERT INTO `admin_menu` VALUES ('2', 'fa-globe', 'На сайт', '?', '0', 'treeview', '');
INSERT INTO `admin_menu` VALUES ('3', 'fa-edit', 'test', '?r=admin%2Ftest', '0', 'treeview', '');
INSERT INTO `admin_menu` VALUES ('4', '', 'MAIN NAVIGATION', null, '0', 'header', '');
INSERT INTO `admin_menu` VALUES ('5', 'fa-home', 'На главную', '?r=admin%2Findex', '0', 'treeview', '');
INSERT INTO `admin_menu` VALUES ('6', 'fa-sliders', 'Меню', '#', '100', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('7', 'fa-home', 'Меню сайта', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('8', 'fa-sliders', 'Админ меню', '?r=admin%2Fmenu', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('9', 'fa-sliders', 'ПРИМЕР МЕНЮ', null, '0', 'header', '');
INSERT INTO `admin_menu` VALUES ('10', 'fa-dashboard', 'Dashboard', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('11', 'fa-circle-o', 'Dashboard v1', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('12', 'fa-circle-o', 'Dashboard v2', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('13', 'fa-files-o', 'Layout Options', '#', '0', 'treeview', 'label-primary');
INSERT INTO `admin_menu` VALUES ('14', 'fa-circle-o', 'Top Navigation', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('15', 'fa-circle-o', 'Boxed', '#', '0', 'treeview-menu', 'label-primary');
INSERT INTO `admin_menu` VALUES ('16', 'fa-circle-o', 'Fixed', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('17', 'fa-circle-o', 'Collapsed Sidebar', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('18', 'fa fa-th', 'Widgets', '#', '0', 'treeview', 'label-info');
INSERT INTO `admin_menu` VALUES ('19', 'fa-pie-chart', 'Charts', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('20', 'fa-circle-o', 'ChartJS', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('21', 'fa-circle-o', 'Morris', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('22', 'fa-circle-o', 'Flot', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('23', 'fa-circle-o', 'Inline charts', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('24', 'fa-laptop', 'UI Elements', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('25', 'fa-circle-o', 'General', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('26', 'fa-circle-o', 'Icons', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('27', 'fa-circle-o', 'Buttons', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('28', 'fa-circle-o', 'Sliders', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('29', 'fa-circle-o', 'Timeline', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('30', 'fa-circle-o', 'Modals', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('31', 'fa-edit', 'Forms', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('32', 'fa-circle-o', 'General Elements', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('33', 'fa-circle-o', 'Advanced Elements', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('34', 'fa-circle-o', 'Editors', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('35', 'fa-table', 'Tables', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('36', 'fa-circle-o', 'Simple tables', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('37', 'fa-circle-o', 'Data tables', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('38', 'fa-calendar', 'Calendar', '#', '0', 'treeview', 'label-danger');
INSERT INTO `admin_menu` VALUES ('39', 'fa-envelope', 'Mailbox', '#', '0', 'treeview', 'label-warning');
INSERT INTO `admin_menu` VALUES ('40', 'fa-folder', 'Examples', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('41', 'fa-circle-o', 'Invoice', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('42', 'fa-circle-o', 'Profile', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('43', 'fa-circle-o', 'Login', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('44', 'fa-circle-o', 'Register', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('45', 'fa-circle-o', 'Lockscreen', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('46', 'fa-circle-o', '404 Error', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('47', 'fa-circle-o', '500 Error', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('48', 'fa-circle-o', 'Blank Page', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('49', 'fa-circle-o', 'Pace Page', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('50', 'fa-share', 'Multilevel', '#', '0', 'treeview', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('51', 'fa-circle-o', 'Level One', '#', '0', 'treeview-menu', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('52', 'fa-circle-o', 'Level One', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('53', 'fa-circle-o', 'Level Two', '#', '0', 'treeview-menu', 'fa-angle-left');
INSERT INTO `admin_menu` VALUES ('54', 'fa-circle-o', 'Level Two', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('55', 'fa-circle-o', 'Level Three', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('56', 'fa-circle-o', 'Level Three', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('57', 'fa-circle-o', 'Level One', '#', '0', 'treeview-menu', '');
INSERT INTO `admin_menu` VALUES ('58', 'fa-book', 'Documentation', '#', '0', 'treeview', '');
INSERT INTO `admin_menu` VALUES ('59', '', 'LABELS', '', '0', 'header', '');
INSERT INTO `admin_menu` VALUES ('60', 'fa-circle-o text-red', 'Important', '', '0', 'treeview', '');
INSERT INTO `admin_menu` VALUES ('61', 'fa-circle-o text-yellow', 'Warning', '', '0', 'treeview', '');
INSERT INTO `admin_menu` VALUES ('62', 'fa-circle-o text-aqua', 'Information', '', '0', 'treeview', '');
SET FOREIGN_KEY_CHECKS=1;
