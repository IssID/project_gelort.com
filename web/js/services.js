'use strict';

var app = angular.module('app');

app.service('LeaseService', function($http) {
    this.get = function() {
        return $http.get('/models/Country');
    };
    this.post = function (data) {
        return $http.post('/models/Country', data);
    };
    this.put = function (id, data) {
        return $http.put('/models/Country' + id, data);
    };
    this.delete = function (id) {
        return $http.delete('/models/Country' + id);
    };
});