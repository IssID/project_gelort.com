<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Texts;
use app\models\User;
use app\models\AdminkaMenu;


class AdminkaController extends Controller
{
    public $layout = 'adminka';
    private $admin_menu;

    // переменные для меню и версия
    function beforeAction($action)
    {
        $query = AdminkaMenu::find();
        $this->admin_menu = $query->orderBy('id')->all();
        Yii::$app->view->params['admin_menu'] = $this->admin_menu; //передаем параметры в layouts
        Yii::$app->view->params['version'] = 'v0.0.2';

        $user = Yii::$app->user->identity;
        if (isset($user->access_token)) {
            Yii::$app->view->params['access_token'] = $user->access_token;
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all Discussion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = User::find();
        $users = $query->orderBy('id')->all();
        $user = Yii::$app->user->identity;
        if (isset($user->access_token) && $user->access_token >= 99) {
            $message = 'Admin Panel';
            $title = " test admin title";
            $text = 'test<br> admin text';
            return $this->render('index', [
                'message' => $message,
                'user' => $user,
                'title' => $title,
                'text' => $text,
                'users' => $users,
                // 'admin_menu' => $this->admin_menu,
            ]);

        } else {
            throw new \yii\web\HttpException(404, 'Ой. У вас нет доступа.'); // выводим ошибку авторизации и 404
        }

    }

    public function actionMenu()
    {
        $user = Yii::$app->user->identity;
        if (isset($user->access_token) && $user->access_token >= 99) {
            $message = 'Настройка меню';
            $query = AdminkaMenu::find();
            $admin_menu = $query->orderBy('id')->all();
            return $this->render('menu', [
                'message' => $message,
                'admin_menu' => $admin_menu,
            ]);

        } else {
            throw new \yii\web\HttpException(404, 'Ой. У вас нет доступа.'); // выводим ошибку авторизации и 404
        }
    }

    public function actionTest()
    {
        $user = Yii::$app->user->identity;
        if (isset($user->access_token) && $user->access_token >= 99) {
            $query = Texts::find();
            $title = 'About';
            $contents = $query->where(["title" => $title])->asArray()->one();
            return $this->render('test', [
                'contents' => $contents,

            ]);

        } else {
            throw new \yii\web\HttpException(404, 'Ой. У вас нет доступа.'); // выводим ошибку авторизации и 404
        }
    }
}
