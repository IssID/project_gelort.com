<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class TestController extends Controller
{
    public function actionIndex($message = 'Главная страница для тестов TestController')
    {
        return $this->render('index', ['message' => $message]);
    }
}
