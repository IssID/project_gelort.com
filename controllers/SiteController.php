<?php

namespace app\controllers;

use app\models\Article;
use app\models\Category;
use app\models\CommentForm;
use app\models\Texts;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ContactForm;

use app\models\EntryForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Показываем главную страницу
     *
     * @return string
     */
    public function actionIndex()
    {
        // получаем все записи модели с пагинацией
        $data = Article::getAll(1);
        // получаем популярные записи
        $popular = Article::getPopular();
        // получаем последние записи
        $recent = Article::getRecent();
        // получаем все категории
        $categories = Category::getAll();

        return $this->render('index', [
            'articles' => $data['articles'],
            'pages' => $data['pages'],
            'popular' => $popular,
            'recent' => $recent,
            'categories' => $categories,
        ]);
    }

    public function actionView($id)
    {
        $article = Article::findOne($id);
        // получаем популярные записи
        $popular = Article::getPopular();
        // получаем последние записи
        $recent = Article::getRecent();
        // получаем все категории
        $categories = Category::getAll();

        $comments = $article->getArticleComments();
        $commentForm = new CommentForm();

        $article->viewedCounter();

        return $this->render('single',[
            'article' => $article,
            'popular' => $popular,
            'recent' => $recent,
            'categories' => $categories,
            'comments' => $comments,
            'commentForm' => $commentForm,
        ]);
    }

    public function actionCategory($id)
    {
        $data = Category::getArticlesByCategory($id);
        // получаем популярные записи
        $popular = Article::getPopular();
        // получаем последние записи
        $recent = Article::getRecent();
        // получаем все категории
        $categories = Category::getAll();

        return $this->render('category',[
            'articles' => $data['articles'],
            'pages' => $data['pages'],
            'popular' => $popular,
            'recent' => $recent,
            'categories' => $categories,
        ]);
    }

    /**
     * Displays contact page.
     * @throws
     * @return Response|string
     */
    public function actionContact()
    {
        throw new \yii\web\HttpException(404, 'Ой. Такой страницы не существует.');

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $title = 'About';
        $query = Texts::find();
        $contents = $query->where(["title" => $title])->asArray()->one();
        return $this->render('about', [
            'contents' => $contents,
        ]);
    }

    public function actionComment($id)
    {
        $model = new CommentForm();
        if(Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            if($model->saveComment($id))
            {
                return $this->redirect(['site/view','id'=>$id]);
            }

        }
    }
    // MY CONTROLLERS ---------------------------------------------------------------------------------------------------------------------------------------------

    public function actionTest($message = 'Тестовая страница')
    {
        if (Yii::$app->user->identity) {
            return $this->render('test', ['message' => $message]);
        } else {
            throw new \yii\web\HttpException(404, 'Oops. Not logged in.');
        }
    }

    /**
     * форма в которую передаем данные и отображаем.
     * @return string
     */
    public function actionEntry()
    {
        $model = new EntryForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // данные в $model удачно проверены

            // делаем что-то полезное с $model ...

            return $this->render('entry-confirm', ['model' => $model]);
        } else {
            // либо страница отображается первый раз, либо есть ошибка в данных
            return $this->render('entry', ['model' => $model]);
        }
    }
}
