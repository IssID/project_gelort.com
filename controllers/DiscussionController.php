<?php

namespace app\controllers;

use Yii;
use app\models\Discussion;
use yii\web\Controller;

use yii\data\Pagination;

class DiscussionController extends Controller
{
    /**
     * Lists all Discussion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $message = 'Discussion';
        $query = Discussion::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $countries = $query->orderBy('id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'message' => $message,
            'countries' => $countries,
            'pagination' => $pagination,
        ]);
    }
}
