<?php
/**
 * Created by PhpStorm.
 * User: IssID
 * Date: 04.08.2018
 * Time: 18:49
 */

namespace app\controllers;

use app\models\SignupForm;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;
use app\models\User;



class AuthController extends Controller
{
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();

        if(Yii::$app->request->isPost)
        {
            $post = Yii::$app->request->post();
            if($post['SignupForm']['confirmPassword'] == $post['SignupForm']['password'])
            {
                $model->load($post);
                if($model->signup()){
                    return $this->redirect(['auth/login']);
                }
            }else{
                echo "Пароли не совпадают";
            }

        }

        return $this->render('signup', [
            'model'=> $model,
    ]);
    }




    public function actionTest()
    {
        $user = User::findOne(1);

        Yii::$app->user->logout();
        var_dump(Yii::$app->user->isGuest);
//        exit;
    }

}