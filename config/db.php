<?php
$config_ini = @parse_ini_file("../../config.ini", true);

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host='.$config_ini['mysql']['dbhost'].';dbname=yii',
    'username' => $config_ini['mysql']['dbuser'],
    'password' => $config_ini['mysql']['dbpasswd'],
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
