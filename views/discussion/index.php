<?php
use yii\helpers\Html; // вывод текст переменной
use yii\widgets\LinkPager; // пагинатор

$this->title = 'Gelort.com'; // название закладки страницы 

?>
<h1><?= Html::encode($message) ?></h1>

<ul>
<?php foreach ($countries as $discussion): ?> 
    <li>
        <?= Html::encode("{$discussion->title} ({$discussion->date})") ?>
        <br>
        <?= $discussion->text ?>
        <?= $discussion->author ?>
    </li>
<?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>