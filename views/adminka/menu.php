<?php
use yii\helpers\Html; // вывод текст переменной
use yii\widgets\LinkPager; // пагинатор

$this->title = 'Gelort.com'; // название закладки страницы 
// $this->menus = 'test';
?>
<h1><?= Html::encode($message) ?></h1> 
<br>


<?php

//echo "<pre>";
// foreach ($admin_menu as $menu){
// 	if(isset($menu['title'])){
// 		echo $menu['title'];
// 		echo "<br>";
// 	}
// }
// 	 print_r($admin_menu);


?>

<ul class="sidebar-menu" >
    <?php
    $menuUl = 0; $menuli = 0;
    foreach ($this->params['admin_menu'] AS $menu):

        if($menu->additional == "label-primary")   { $menu->additional = "<span class=\"label pull-right label-primary\">{{4+2}}</span>"; }
        else if($menu->additional == "label-default")   { $menu->additional = "<span class=\"label pull-right label-default\">1</span>"; }
        else if($menu->additional == "label-success")   { $menu->additional = "<span class=\"label pull-right label-success\">2</span>"; }
        else if($menu->additional == "label-warning")   { $menu->additional = "<span class=\"label pull-right label-warning\">3</span>"; }
        else if($menu->additional == "label-info")      { $menu->additional = "<span class=\"label pull-right label-info\">new</span>"; }
        else if($menu->additional == "label-danger")    { $menu->additional = "<span class=\"label pull-right label-danger\">4</span>"; }
        else if($menu->additional == "fa-angle-left")   { $menu->additional = "<i class=\"fa fa-angle-left pull-right\"></i>"; }
        else { $menu->additional = ''; }

        if($menu->class == 'header'){ if($menuUl == 1){ $menuUl = 0; echo "</ul>"; } echo "<li class=\"header\">".$menu->title."</li>"; } // разделители

        // главное меню
        if($menu->class == 'treeview'){
            if($menuUl == 1){ $menuUl = 0; echo "</ul>"; } // если главное меню а ранее было подменю закрываем подменю
            $menuli = 1; // зашли в главное меню

            if(isset($this->params['access_token']) && $this->params['access_token'] >= $menu->access_level){
                $off_menu = 0;
                echo "
                            <li class=\"treeview \">
                            <a href=\"".$menu->url."\"><i class=\"fa  ".$menu->icon."\"></i><span>".$menu->title."</span> ".$menu->additional."</a>";
            }else{
                $off_menu = 1; // отключить подменю из дом
            }
        }
        // подменю
        if($menu->class == 'treeview-menu' && $menuUl == 0){ // если подменю небыло открыто ранее
            $menuli = 0; $menuUl = 1;
            echo "<ul class=\"treeview-menu\">";
        }
        if($menu->class == 'treeview-menu' && $menuUl == 1){
            if(isset($this->params['access_token']) && $this->params['access_token'] >= $menu->access_level && $off_menu == 0){
                echo "<li><a href=\"".$menu->url."\"><i class=\" fa ".$menu->icon."\"></i>".$menu->title."</a>".$menu->additional."</li>";
            }
        }

    endforeach;
    if($menuUl == 1){
        echo "</ul>";
    }
    echo "</li>";
    ?>
</ul>
