<?php

/* @var $this yii\web\View */

use yii\helpers\Html; // <?= Html::encode($this->title) ? >
use yii\jui\DatePicker; // <?= DatePicker::widget(['name' => 'date']) ? >

$this->title = $contents['title'];

$this->params['breadcrumbs'][] = $this->title;

?>
<?php
$js = <<<JS
 $('form').on('beforeSubmit', function(){
 alert('Работает!');
 return false;
 });
JS;

$this->registerJs($js);
?>

<code>code</code>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $contents['body']; ?>
    <?php //    <code><?= __FILE__ ? ></code> ?>
</div>
