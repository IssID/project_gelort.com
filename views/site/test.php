<?php

use yii\helpers\Html;

?>
<?= Html::encode($message) ?>

<?php
echo Yii::$app->urlManager->createUrl(['country']);
?>
<p><a class="btn btn-default" href="https://www.yiiframework.com/doc/guide/2.0/ru">Полное руководство по Yii 2.0
        &raquo;</a></p>
<p><a class="btn btn-default" href="<?php echo Yii::$app->urlManager->createUrl(['test']); ?>">TestController
        &raquo;</a></p>
<p><a class="btn btn-default" href="<?php echo Yii::$app->urlManager->createUrl(['discussion']); ?>">discussion
        &raquo;</a></p>

<!-- <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/" target="_blank">Yii Documentation &raquo;</a></p> -->
<!-- <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/" target="_blank">Yii Forum &raquo;</a></p> -->
<!-- <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/" target="_blank">Yii Extensions &raquo;</a></p> -->
<br>

<p><a class="btn btn-default" href="https://www.yiiframework.com/doc/guide/2.0/ru/start-forms">Работа с формами
        &raquo;</a>
    <a class="btn btn-default" href="<?php echo Yii::$app->urlManager->createUrl(['site/entry']); ?>">Пример Entry
        &raquo;</a></p>

<p><a class="btn btn-default" href="https://www.yiiframework.com/doc/guide/2.0/ru/start-databases">Работа с базами
        данных &raquo;</a>
    <a class="btn btn-default" href="<?php echo Yii::$app->urlManager->createUrl(['country']); ?>">Пример Country
        &raquo;</a></p>

