<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\BootstrapPluginAsset;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" data-ng-app="app">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<div class="wrap">
<!--     <aside class="sidebar-top">
        <nav class="navbar navbar-inverse ">

        </nav>
    </aside> -->

    <aside class="sidebar-left">
      <nav class="navbar navbar-inverse ">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="sidebar-menu">
            <?php
                $menuUl = 0; $menuli = 0;
                foreach ($this->params['admin_menu'] AS $menu):

                         if($menu->additional == "label-primary")   { $menu->additional = "<span class=\"label pull-right label-primary\">{{4+2}}</span>"; }
                    else if($menu->additional == "label-default")   { $menu->additional = "<span class=\"label pull-right label-default\">1</span>"; }
                    else if($menu->additional == "label-success")   { $menu->additional = "<span class=\"label pull-right label-success\">2</span>"; }
                    else if($menu->additional == "label-warning")   { $menu->additional = "<span class=\"label pull-right label-warning\">3</span>"; }
                    else if($menu->additional == "label-info")      { $menu->additional = "<span class=\"label pull-right label-info\">new</span>"; }
                    else if($menu->additional == "label-danger")    { $menu->additional = "<span class=\"label pull-right label-danger\">4</span>"; }
                    else if($menu->additional == "fa-angle-left")   { $menu->additional = "<i class=\"fa fa-angle-left pull-right\"></i>"; }
                    else { $menu->additional = ''; }

                    if($menu->class == 'header'){ if($menuUl == 1){ $menuUl = 0; echo "</ul>"; } echo "<li class=\"header\">".$menu->title."</li>"; } // разделители

                    // главное меню
                    if($menu->class == 'treeview'){
                        if($menuUl == 1){ $menuUl = 0; echo "</ul>"; } // если главное меню а ранее было подменю закрываем подменю
                        $menuli = 1; // зашли в главное меню

                        if(isset($this->params['access_token']) && $this->params['access_token'] >= $menu->access_level){
                            $off_menu = 0;
                            echo "
                            <li class=\"treeview \">
                            <a href=\"".Yii::$app->urlManager->createUrl([$menu->url])."\"><i class=\"fa  ".$menu->icon."\"></i><span>".$menu->title."</span> ".$menu->additional."</a>";
                        }else{
                            $off_menu = 1; // отключить подменю из дом
                        }
                    }
                    // подменю
                    if($menu->class == 'treeview-menu' && $menuUl == 0){ // если подменю небыло открыто ранее
                        $menuli = 0; $menuUl = 1;
                        echo "<ul class=\"treeview-menu\">";
                    }
                    if($menu->class == 'treeview-menu' && $menuUl == 1){
                        if(isset($this->params['access_token']) && $this->params['access_token'] >= $menu->access_level && $off_menu == 0){
                            echo "<li><a href=\"".Yii::$app->urlManager->createUrl([$menu->url])."\"><i class=\" fa ".$menu->icon."\"></i>".$menu->title."</a>".$menu->additional."</li>";
                        }
                    }

                endforeach;
                if($menuUl == 1){
                    echo "</ul>";
                }
                echo "</li>";
            ?>
            </ul> 
            <?php
            // Logout
                echo Nav::widget([
                    'options' => ['class' => ''],
                    'items' => [
                        (
                        '<li class="treeview">'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Logout (' . Yii::$app->user->identity->username . ') <i class="fa fa-sign-out"></i> ',
                            ['class' => 'btn btn-link logout ']
                        )
                        . Html::endForm()
                        . '</li>'
                        )
                    ],
                ]);
            ?>
          </div>
          <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
      </nav>
            <div class="container-fluid " style="background-color:#ccc;">
                <div class="container">
                    <a href="https://vk.com/issid"><i class="fa fa-vk"></i></a> 
                    <a href="https://t.me/IssID"><i class="fa fa-telegram"></i></a> 
                    <a href="https://gitlab.com/IssID"><i class="fa fa-gitlab"></i></a> 
                    <?php echo $this->params['version']; ?>
                    <br>
                    &copy; My Company <?= date('Y') ?>
                </div>
            </div>
    </aside>
    <div class="content">
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

<?php $this->endBody() ?>
<script> $('.sidebar-menu').SidebarNav() </script>
</body>
</html>
<?php $this->endPage() ?>
