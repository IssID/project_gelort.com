<?php
/**
 * Created by PhpStorm.
 * User: IssID
 * Date: 04.08.2018
 * Time: 20:36
 */

namespace app\models;

use yii\base\Model;
use app\models\User;


class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $confirmPassword;

    public function rules()
    {
        return [
            [['username', 'email', 'password','confirmPassword'], 'required'],
            [['username'], 'string'],
            [['email'], 'email'],
//            ['password', 'compare', 'compareAttribute'=>'confirmPassword'],   // сравниваем ервый пароль со вторым
            ['confirmPassword', 'compare', 'compareAttribute'=>'password'],     // сравниваем второй пароль с первым
            [['email'], 'unique', 'targetClass' => 'app\models\User', 'targetAttribute'=>'email']
        ];
    }

    public function signup()
    {
        if($this->validate())
        {
            $user = new User();
            $this->password = md5($this->password); // шифруем пароль
            $user->attributes = $this->attributes;
            return $user->create();
        }
    }

}