<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageUpload extends Model
{
    public $image;

        public function rules()
        {
            return [
                [['image'], 'required'],
                [['image'], 'file', 'extensions' => 'jpg,png']
            ];
        }

    public function uploadFile(UploadedFile $file, $currentImage)
    {
        $this->image = $file;

        if($this->validate())
        {
            $this->deleteCurrentImage($currentImage);
            // возращаем имя
            return $this->saveImage();
        }
    }

    public function getFolder()
    {
        return Yii::getAlias('@web') . 'uploads/';
    }

    public function generateFileName()
    {
        return $filename = strtolower(md5(uniqid($this->image->baseName))) . "." .$this->image->extension;
    }

    public function deleteCurrentImage($currentImage)
    {
        if($this->fileExists($currentImage))
        {
            // удаляем старый файл
            unlink($this->getFolder() . $currentImage);
        }
    }

    public function fileExists($currentImage)
    {
        // проверяем на наличие записи в базе и наличии файла в директории
        if(!empty($currentImage) && $currentImage != null)
        {
            return is_file($this->getFolder() . $currentImage);
//            return file_exists($this->getFolder() . $currentImage);
        }
    }

    public function saveImage()
    {
        // создаем уникальное имя для картинки
        $filename = $this->generateFileName();

        // Загружаем картинку на сервер в деректорию приложения
        $this->image->saveAs($this->getFolder() . $filename);

        return $filename;
    }
}