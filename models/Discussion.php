<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "discussion".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $date
 * @property string $author
 * @property string $img
 */
class Discussion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'discussion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['title', 'text', 'author', 'img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'date' => 'Date',
            'author' => 'Author',
            'img' => 'Img',
        ];
    }
}
