<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin_menu".
 *
 * @property int $id
 * @property string $icon
 * @property string $title
 * @property string $url
 * @property int $access_level
 * @property string $class
 * @property string $additional
 */
class AdminMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['access_level'], 'integer'],
            [['icon', 'title', 'url', 'class', 'additional'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'icon' => 'Icon',
            'title' => 'Title',
            'url' => 'Url',
            'access_level' => 'Access Level',
            'class' => 'Class',
            'additional' => 'Additional',
        ];
    }
}
