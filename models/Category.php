<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $title
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * объявляем связь таблиц
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['category_id' => 'id']);
    }

    /**
     * получаем количество записей в категории
     * @return int|string
     */
    public function getArticlesCount()
    {
        return $this->getArticles()->where(['status'=>1])->count();
    }

    /**
     * получаем все записи категорий
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAll()
    {
        return Category::find()->all();
    }

    public static function getArticlesByCategory($id)
    {
        // получаем модель
        $query = Article::find()->where(['category_id' => $id, 'status'=>1]);
        // клонируем (видимо для того что бы не делать повторный запрос в базу)
        $countQuery = clone $query;
        // собственно сама пагинация (полное количество объектов, сколько выводить на страницу)
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 6]);

        $articles = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $data['articles'] = $articles;
        $data['pages'] = $pages;

        return $data;
    }
}
